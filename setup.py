#!/usr/bin/python
from setuptools import setup

setup(
    name = "terbium",
    version = "0.5",
    packages = [ 'terbium', 'terbium.tools' ],
    include_package_data = True,
    install_requires = [ 'PIL', 'simplejson', 'jinja2' ],
    package_data = {
        'terbium': [ 'customizations.txt'
            , 'terbium.json'
            , 'templates/terbium/*.html'
            , 'templates/terbium/assets/*.css'
            , 'templates/terbium/assets/*.png'
            , 'templates/terbium/assets/*.jpg'
            , 'templates/terbium/assets/*.gif'
            , 'tools/video-thumbnails.sh' ]
        , '' : [ 'README', 'COPYING' ],
    },
    author = "Shawn Sulma",
    author_email = "shawn@terbium.ca",
    description = "A static file (image/audio/video) gallery generator",
    license = "GPL",
    keywords = "photo album gallery static",
    url = "http://terbium.ca/",
    long_description="""Terbium is a small Python application that turns a directory structure of image and media files into a web-browseable gallery. It generates .html files in a self-contained hierarchy, which makes it a good candidate for browseable CD/DVDs of images and media files, or for online hosted galleries that do not need fancy validation, authentication or access patterns.""",
    classifiers=[
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Programming Language :: Python",
        "Development Status :: 3 - Alpha",
        "Intended Audience :: End Users/Desktop",
        "Topic :: Multimedia",
        "Operating System :: POSIX",
        "Environment :: Console"
      ],
    entry_points = {
        'console_scripts': [
            'terbium = terbium.gallery:generate'
        ],
    }
    # could also include download_url
)
