# Terbium

Terbium is an application for generating static galleries of images or other 
media files.  It creates a directory structure matching the original structure
of a "source" directory.  This "target" directory is populated with 
self-contained HTML files, preview-sized thumbnails of the files in the 
original source, and copies of those source files.  The result is browseable 
in any modern graphical browser.

Terbium's home page is at [http://terbium.ca](http://terbium.ca)

## Installation

Terbium can be downloaded from [https://bitbucket.org/unayok/terbium/wiki/Downloads](https://bitbucket.org/unayok/terbium/wiki/Downloads) . It comes as 
either a tar.gz file, or as a Python .egg.

To install the .egg, you run a command similar to the following:

```
$ easy_install <filename>
```

If you have setuptools installed, you can also install from the .tar.gz by 
untarring it some place then running

```
$ ./setup.py install
```

In the installation directory.

If you do not have setuptools or disttools installed, you can extract the 
contents of the tar.gz file and run the scripts directly from the directory.  
Note that the "terbium" directory containing "gallery.py" must be either in 
the starting directory on on the PYTHONPATH for this to work, and you should 
replace the "terbium" command listed below with "terbium/gallery.py"

## Dependencies

Terbium makes use of the Python Imaging Library (PIL), JSON-py, and Jinja2.  
easy_install will ensure that these dependencies are met if your operating 
system's package repository does not contain installation packages for these.  
You can also find them at [http://www.python.org/pypi/](http://www.python.org/pypi/) .

## Usage

The simplest usage is to structure your data files (images, video and audio) 
in a "source" directory, in the hierarchy you want to see in the output.  
Determine your "target" directory's name, and run the following command line:

```
$ terbium <source-dir> <target-dir>
```

where <source-dir> is the top-level directory of your source files, and 
<target-dir> is the directory in which the output should be built.

By itself, this uses a completely default configuration for the output.  You 
may want to alter this configuration.  This is documented more on the Terbium 
wiki at [http://bitbucket.org/unayok/terbium/wiki/Documentation](http://bitbucket.org/unayok/terbium/wiki/Documentation) .

## Source

The source code for Terbium is available as a tarball or via source control.  
See [http://bitbucket.org/unayok/terbium/src](http://bitbucket.org/unayok/terbium/src) for more details.

## License

Terbium is copyright by Shawn Sulma (shawn at terbium.ca) and is licensed under 
the GNU General Public License v3 (or later).  Please see the file COPYING 
for more details.
