#    terbium/template.py - template extras for the terbium static gallery generator
#    Copyright (C) 2009 Shawn Sulma <shawn@terbium.ca> http://terbium.ca/
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from jinja2 import nodes
from jinja2.ext import Extension
import re
import itertools

class TextExtension(Extension):
    """A Jinja2 extension for performing some text substitutions.  This is a naive attempt at a minimal project/site localization mechanism."""
    # a set of names that trigger the extension.
    tags = set(['text'])
    def __init__(self, environment):
        Extension.__init__( self, environment )
        self.environment = environment

    def callback( self, caller ) :
        text = caller().strip()
        if text in self.environment.text :
            return self.environment.text[text]
        else :
            return text

    def parse(self, parser):
        # the first token is the token that started the tag.
        lineno = parser.stream.next().lineno

        # parse the body of the cache block up to the end tag and "drop the needle" (the end tag)`endcache`
        body = parser.parse_statements(['name:endtext'], drop_needle=True)

        # now return a `CallBlock` node that calls our helper method on this extension.
        return nodes.CallBlock(self.call_method('callback', []), [], [], body).set_lineno(lineno)

class CleanExtension(Extension):
    """A Jinja2 extension for performing some text "cleanup" within a template.  Currently this is limited to surrounding hyphens with a space on either side, converting underscores to spaces and stripping off excess whitespace."""
    # a set of names that trigger the extension.
    tags = set(['clean'])
    def __init__(self, environment):
        Extension.__init__( self, environment )
        self.environment = environment

    hyphenRE = re.compile( ur'([^ ])-([^ ])' )
    def callback( self, caller ) :
        text = unicode( caller() )
        text = text.strip()
        text = text.replace(u'_', u' ')
        return re.sub( self.hyphenRE, ur"\1 - \2", text )

    def parse(self, parser):
        lineno = parser.stream.next().lineno
        body = parser.parse_statements(['name:endclean'], drop_needle=True)
        return nodes.CallBlock(self.call_method('callback', []), [], [], body).set_lineno(lineno)

def filter_limit( value, limit ) :
    return itertools.islice( value, 0, limit )

