#    terbium/util.py - utility methods for the terbium static gallery generator
#    Copyright (C) 2009 Shawn Sulma <shawn@terbium.ca> http://terbium.ca/
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os.path
from functools import wraps

# decorators
def requireHeader( func ) :
    @wraps( func )
    def decorated( self, *arg, **kwarg ) :
        if not hasattr( self, "header" ) :
            self.loadHeader()
        return func( self, *arg, **kwarg )
    return decorated

def requireActual( func ) :
    @wraps( func )
    def decorated( self, *arg, **kwarg ) :
        if not hasattr( self, "actual" ) :
            self.resolveActual()
        return func( self, *arg, **kwarg )
    return decorated

def memoize( func ) :
    cache = {}
    @wraps( func )
    def decorated( *args ) :
        try :
            return cache[args]
        except KeyError :
            cache[args] = value = func( *args )
            return value
        except TypeError :
            return func( *args )
    return decorated

# metadata handling
import codecs
def loadText( name ) :
    text = {}
    if os.path.isfile( name ) :
        f = codecs.open( name, "rb", "utf-8" )
        for line in f :
            line = line.strip()
            if line and line[0] != u'#' :
                k, v = line.split( u'=', 1 )
                text[k.strip()] = v.strip()
    return text

def safeBool( value ) :
    if value is True or value is False:
        return value
    value = unicode( value ).strip().lower()
    return value in ( u'true', u't', u'y', u'1', u'on', u'yes' )

import xml.parsers.expat
class MetadataXMLHandler( object ) :
    def __init__( self, f ) :
        self.metadata = {}
        self.data = None
        self.field = None
        self.parser = xml.parsers.expat.ParserCreate()
        self.parser.StartElementHandler = self.start_element
        self.parser.EndElementHandler = self.end_element
        self.parser.CharacterDataHandler = self.char_data
        self.parser.ParseFile( f )

    def start_element( self, name, attrs ) :
        if name == "field" :
            self.data = []
            self.field = attrs['name']
    def char_data( self, data ) :
        if self.field :
            self.data.append( data.strip() )
    def end_element( self, name ) :
        if name == "field"  :
            data = "".join( self.data )
            if len( data.strip() ) > 0 :
                self.metadata[self.field] = decode( data )
            self.data = None
            self.field = None

# text handling

def decode( s ) :
    if not isinstance( s, unicode ):
        try:
            s = unicode( s, 'utf-8')
        except UnicodeDecodeError :
            try :
                s = unicode( s, 'iso-8859-1' )
            except UnicodeDecodeError :
                pass # Lazy, but what can we do?
    return s

# collation

def collator( definition ) :
    collators = []
    for k in definition.split( ',' ) :
        k = k.strip()
        collators.append( keycollator( k ) )
    def comparator( left, right ) :
        for c in collators :
            r = c( left, right )
            if r != 0 :
                return r
        return 0
    return comparator

def keycollator( key ) :
    if not key :
        raise ValueError, "missing collator key"
    if key[0] == u'-' :
        direction = -1
        key = key[1:]
    else :
        direction = 1
        if key[0] == u'+' :
            key = key[1:]
    def inner( left, right ) :
        return direction * cmp( left.collationValue( key ), right.collationValue( key ) )
    return inner

def formatText( text ) :
    if u'\n' in text :
        text = text.replace( '\n', '</p><p>' )
        if text[-7:] == "</p><p>" :
            text = text[:-7]
    return "<p>" + text + "</p>"
