#    terbium/__init__.py - static gallery generation
#    Copyright (C) 2009 Shawn Sulma <shawn@terbium.ca> http://terbium.ca/
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os.path
import re
import itertools
import time
import shutil
import codecs
from posixpath import normpath
from datetime import datetime
from ConfigParser import ConfigParser
import logging
try :
    import simplejson as json
except ImportError :
    import json
import hashlib

import PIL.Image
import PIL.ImageFile
from PIL.ExifTags import TAGS
# PIL HACKS
PIL.ImageFile.MAXBLOCK = 1024*1024

# extra EXIF tags that PIL doesn't seem aware of
TAGS.update( { 0xfdea : 'Lens'
    , 0xafc1 : 'ExpandLens'
    , 0xc630 : 'DNGLensInfo'
    , 0xb027 : 'LensID'
    , 0xbfc7 : 'LensID'
    , 0x9002 : 'Aperture'
    , 0x9003 : 'OriginalDateTime'
    , 0x4746 : 'RatingStars'
    , 0x4749 : 'RatingPercent'
    } )

# relax older-than/newer-than checks by a couple of seconds so FAT-based timestamps are still useful.
MTIME_WINDOW = 2


def get_exif_tag ( tag ) :
    return TAGS.get( tag, tag ) # EXIF_TAGS_EXTRA.get( tag, tag ) )

import terbium.util
from terbium.util import safeBool, requireActual, requireHeader, memoize, formatText


logger = logging.getLogger ( "terbium" )

class IgnoreMe ( Exception ) :
    pass

def findEntry( map, *alts ) :
    for alt in alts :
        if alt in map :
            return map[alt]
    raise KeyError, str(alts) + " could not be resolved"

def isFileOlder( mtime, expected ) :
    if not os.path.exists( expected ) :
        return True
    d_stat = os.stat( expected )
    if int( mtime ) - MTIME_WINDOW > int( d_stat.st_mtime ) :
        return True

def isFileNewer( mtime, expected ) :
    if not os.path.exists( expected ) :
        return True
    d_stat = os.stat( expected )
    if int( mtime ) + MTIME_WINDOW < int( d_stat.st_mtime ) :
        return True

def isFileDifferent( one, two ) :
    # don't really need a secure hash as this is a naive check.
    with open(one,'rb') as f :
        m = hashlib.md5()
        while True :
            b = f.read( 4096 )
            if b :
                m.update( b )
            else :
                break
        one_hash = m.digest()
    with open(two,'rb') as f :
        m = hashlib.md5()
        while True :
            b = f.read( 4096 )
            if b :
                m.update( b )
            else :
                break
        two_hash = m.digest()
    return one_hash != two_hash

def loadMetadata( meta, base ) :
    f = base + '.xml'
    if os.path.exists( f ) and os.path.isfile( f ) :
        meta['mtime'] = max ( meta.get( 'mtime', 0 ), os.stat( f ).st_mtime )
        xmlfile = file( f )
        try :
            parser = terbium.util.MetadataXMLHandler( xmlfile )
        finally :
            xmlfile.close()
        meta.update( parser.metadata )
    f = base + '.properties'
    if os.path.exists( f ) and os.path.isfile( f ) :
        meta['mtime'] = max( meta.get( 'mtime', 0 ), os.stat( f ).st_mtime )
        properties = ConfigParser()
        properties.readfp( codecs.open( f, 'rb', 'utf-8' ) )
        meta.update( properties.items( "meta" ) )


class Item ( object ) :
    def __init__( self, base ) :
        self.meta = {}
        self.meta['mtime'] = 0
        self.updateMeta( base )

    def updateMeta( self, base ) :
        loadMetadata( self.meta, base )

    def collationValue( self, key ) :
        try :
            if key == "weight" :
                value = findEntry( self.meta, 'weight', 'sort-order' )
            else :
                value = self.meta[key]
            try :
                return float( value )
            except TypeError :
                return value
            except ValueError :
                return value
        except KeyError :
            return 0

    def previousItems( self, limit = None ) :
        if self.parent :
            oc = self.parent.items()
            i = oc.index( self )
            j = limit and ( i - limit ) or 0
            while j < i :
                if j >= 0 :
                    yield oc[j]
                j += 1

    def nextItems( self, limit = None ) :
        if self.parent :
            oc = self.parent.items()
            i = oc.index( self ) + 1
            while i < len( oc ) and ( limit is None or limit > 0 ):
                yield oc[i]
                i += 1
                if limit :
                    limit -= 1

    @memoize
    def getPreviousItem( self ) :
        try :
            return self.previousItems( 1 ).next()
        except StopIteration :
            return None

    @memoize
    def getNextItem( self ) :
        try :
            return self.nextItems( 1 ).next()
        except StopIteration :
            return None

    def isHidden( self ) :
        if 'hidden' in self.meta :
            return safeBool( self.meta['hidden'] )
        if 'ignore' in self.meta :
            return 'hid' == self.meta['ignore'].lower()[:3]
        return False

    def getDisplayName( self ) :
        if 'title' in self.meta :
            return self.meta['title']
        return self.outputName

    def getDescription( self ) :
        if 'description' in self.meta :
            return formatText( self.meta['description'] )

    def getStyleClass( self ) :
        return self.filetype

    def pathToTop( self ) :
        return self.parent.pathToTop()

    def generateFromTemplate ( self, templateName, outFile, context = None ) :
        context = context or self;
        root = (self.parent and self.parent.root or self)
        t = root.templateEngine.get_template( root.getTemplateFilePath( templateName ) )
        output = t.render( item = context, config = root.config )
        f = codecs.open( outFile, "wb", "utf-8" )
        f.write( output )
        f.close()

    def preview_item ( self ) :
        d = { 'name' : self.name
            , 'small' : self.thumbnailURL
            , 'large' : self.itemURL
            , 'overlay' : self.overlayIcon
            , 'panorama' : self.isPanorama
            , 'url' : self.pageURL
        }
        return d

    def full_item ( self ) :
        d = self.preview_item()
        d.update( {
            'description' : self.description
            , 'properties' : self.properties
            , 'additional_properties' : self.additionalProperties
        })
        return d

    def json_item ( self, num_highlights = 1 ) :
        global json
        return json.dumps( self.full_item() )

    def getAssetOutputPath ( self ) :
        return os.path.join( self.pathToTop(), ".assets" )

    hidden = property( isHidden )
    description = property( getDescription )
    previousItem = property( getPreviousItem )
    nextItem = property( getNextItem )
    name = property( getDisplayName )
    thumbnailURL = None
    itemURL = None
    overlayIcon = None
    isPanorama = False
    pageURL = None
    styleClass = property( getStyleClass )
    assetOutputPath = property( getAssetOutputPath )

class Folder ( Item ) :
    filetype = 'folder'
    def __init__ ( self, path, parent = None ) :
        self.sourceName = path
        root = parent and parent.root or self
        self.sourcePath = (parent or root).getSourcePath( path )
        self.outputName = root.trimPrefix( path )
        self.relativePath = (parent or root).getRelativePath( self.outputName )
        self.outputPath = (parent or root).getOutputPath( self.outputName )
        self.root = root
        self.parent = parent
        self.folderItems = []
        self.fileItems = []
        self.relatedItems = []
        self.ignored = []
        metaPath = self.getSourcePath( 'album' )
        logger.debug( "scanning %s", metaPath )
        Item.__init__( self, metaPath )
        if 'ignore' in self.meta and ( safeBool( self.meta['ignore'] ) or self.meta['ignore'][:6] == 'ignore' ) :
            raise IgnoreMe()
        for item in os.listdir( self.sourcePath ) :
            itempath = self.getSourcePath( item )
            try :
                handle_item( self, item, itempath )
            except IgnoreMe :
                self.ignored.append( item )
        if parent and len( self.folderItems ) + len( self.fileItems ) + len( self.relatedItems ) > 0:
            parent.addFolder( self )

    def __getitem__( self, key ) :
        # naive implementation.  Since this is intended to be called infrequently on small sets, this may suffice for now
        for folder in self.folders :
            if folder.sourceName == key :
                return folder
        for f in self.files :
            if f.sourceName == key :
                return f
        raise KeyError, key

    def _get( self, key ) :
        # also a naive implementation, but does not rely on memoization, so it may handle unresolved references better.
        for item in itertools.chain( self.folderItems, self.fileItems, self.relatedItems ) :
            if item.sourceName == key :
                return item
        raise KeyError, key

    def getSourcePath( self, path ) :
        return os.path.join( self.sourcePath, path )

    def getOutputPath( self, path ) :
        return os.path.join( self.outputPath, path )

    def getRelativePath( self, path ) :
        return self.relativePath + path + ( path and "/"  or "" )

    def addFolder( self, folder ) :
        self.folderItems.append( folder )
        self.root.dirCount += 1

    def addFile( self, file ) :
        self.fileItems.append( file )
        self.root.fileCount += 1

    def addRelated( self, related ) :
        self.relatedItems.append( related )

    @memoize
    def pathToTop( self ) :
        return self.pathTo( self.root )

    def pathTo( self, dest ) :
        p = []
        node = self
        while node and node != dest :
            p.append( "../" )
            node = node.parent
        return "".join( p )

    def collationValue( self, key ) :
        if key == "name" :
            return os.path.basename( self.sourcePath )
        if key == "date" :
            # possible complications if date is in meta?  Not for folders for now.
            return os.stat( self.sourcePath ).st_mtime
        if key == "size" :
            return len( self.folders ) + len( self.files )
        return Item.collationValue( self, key )

    @memoize
    def collator( self ) :
        if 'sort-order' in self.meta :
            o = self.meta['sort-order']
        else :
            o = self.root.defaultSortOrder
        return terbium.util.collator( o )

    def _order( self, collection ) :
        return sorted( ( item for item in collection if not item.isHidden() ) , self.collator() )

    @memoize
    def getFolders( self ) :
        return self._order( self.folderItems )

    @memoize
    def getFiles( self ) :
        return self._order( self.fileItems )

    @memoize
    def getRelated( self ) :
        return self._order( self.relatedItems )

    def items( self, limit = None ) :
        return list( itertools.islice( itertools.chain( self.folders, self.files, self.related ), 0, limit ) )

    def preview_items( self, limit = None ) :
        t = self.thumbnailURL
        o = self.outputURL
        return itertools.islice( ( x for x in itertools.chain( self.folders, self.files, self.related ) if t != o + x.thumbnailURL ), 0, limit )

    def breadcrumbs( self ) :
        crumbs = []
        parent = self.parent
        while parent :
            crumbs.append( parent )
            parent = parent.parent
        crumbs.reverse()
        for crumb in crumbs :
            path = ( crumb != self.root and "../" or "" ) + self.pathTo( crumb )
            yield normpath( path + crumb.iconURL ), normpath( path + crumb.pageURL ), crumb.name
        rp = ( self != self.root and "../" or "" )
        try :
            yield normpath( rp + self.iconURL ), normpath( rp + self.pageURL ), self.name
        except TypeError :
            logger.error( "%s has inconsistent data: %s, %s", self.outputName, self.iconURL, self.pageURL )
            raise

    def findPath( self, targetPath ) :
        path = targetPath.split( '/', 1 )
        if len( path ) > 1 :
            if path[0] == '..' :
                next = (self.parent or self)
            else :
                next = None
                for folder in self.folderItems :
                    if folder.sourceName == path[0] :
                        next = folder
                        break
                if not next :
                    for related in self.related :
                        if related.referenceName == path[0] :
                            next = related
                            break
            if next :
                return next.findPath( path[1] )
        else :
            for file in self.files :
                if file.sourceName == path[0] :
                    return file
            for folder in self.folders :
                if folder.sourceName == path[0] :
                    return folder
            for related in self.related :
                if related.referenceName == path[0] :
                    return related
        return None


    def highlighted( self ) :
        r = None
        try :
            chosen = findEntry( self.meta, "highlight", "sample", "sampleimage" )
            r = self.findPath( chosen )
            r = r and r.highlighted()
        except KeyError :
            pass
        except :
            logger.error( "determine highlight failed", exc_info = sys.exc_info() )
            logger.debug( "found %s", self.findPath( chosen ) )
        if not r :
            if len( self.files ) > 0 :
                r = self.files[0]
            elif len( self.folders ) > 0 :
                r = self.folders[0].highlighted()
            elif len( self.related ) > 0 :
                r = self.related[0].highlighted()
        return r

    def buildPath( self, item, basename ) :
        path = []
        parent = item
        while parent and parent != self :
            parent = parent.parent
            if parent :
                path.append( parent.outputURL )
        path.reverse()
        path.append( basename )
        return "".join( path )

    def getThumbnailURL( self ) :
        h = self.highlighted()
        if h :
            return self.buildPath( h, h.thumbnailURL )

    def getIconURL( self ) :
        h = self.highlighted()
        if h :
            return self.buildPath( h, h.iconURL )

    def getPageFile( self ) :
        return 'index.html'

    def getPageURL( self ) :
        return self.outputURL + self.pageFile

    def getExpectedFiles( self ) :
        return self.root.getTypeTemplates( self.filetype ).values()

    def getOutputURL( self ) :
        return self.outputName + "/"

    def getSubtitle( self ) :
        if 'subtitle' in self.meta :
            return formatText( self.meta['subtitle'] )

    def getCopyright( self ) :
        if 'copyright' in self.meta:
            return self.meta['copyright']
        if self.parent and self.parent != self :
            return self.parent.copyright
        return self.root.defaultCopyright

    def getFeedback( self ) :
        try :
            return self.meta['feedback']
        except KeyError :
            if self.parent and self.parent != self:
                return self.parent.feedback
        return self.root.defaultFeedback

    def getGeneratedStylesheets ( self ) :
        a = self.root.config.get( 'generated-stylesheets', {} )
        if not a or len( a ) == 0 :
            logger.debug( str( self.root.config ) )
            raise ValueError
        return a

    @memoize
    def checkDirty( self ) :
        isDirty = False
        index = os.path.join( self.outputPath, self.pageFile )
        if not os.path.exists( index ) :
            logger.debug( self.outputPath + " does not exist" )
            isDirty = True
        elif int( os.stat( index ).st_mtime ) < int( self.meta['mtime'] ):
            logger.debug( self.outputPath + " is out of date (meta) %s ? %s", os.stat( index ).st_mtime, self.meta['mtime'] )
            isDirty = True
        else :
            for c in itertools.chain ( self.folderItems, self.fileItems, self.relatedItems ) :
                if c.isDirty :
                    logger.debug( self.outputPath + " child %s is dirty", c.name )
                    isDirty = True
                    break
            if not isDirty :
                for i in self.cruft() :
                    logger.debug( self.outputPath + " has cruft %s", i )
                    isDirty = True
                    break
        if not isDirty :
            self.root.dirCount -= 1
        return isDirty

    def ensureOutputPath( self ) :
        if not os.path.exists( self.outputPath ) :
            os.makedirs( self.outputPath )

    def deploy( self ) :
        _dirDeployed = self.root.dirDeployed
        if self.isDirty :
            self.ensureOutputPath()
            # First, ensure all children are properly deployed.
            for child in itertools.chain ( self.folderItems, self.fileItems ) :
                child.deploy()
            # children are deployed, now can create index.
            type_templates = self.root.getTypeTemplates( self.filetype )
            for template, outfile in type_templates.iteritems() :
                self.generateFromTemplate( template, self.getOutputPath( outfile ) )
            self.preen( exclude = type_templates.values() )
            self.root.dirDeployed += 1
        if _dirDeployed != self.root.dirDeployed :
            logger.info( "%d%% complete [%d of %d]", float ( ( self.root.dirDeployed * 1000 ) / max( self.root.dirCount, 1 ) ) / 10 , self.root.dirDeployed, self.root.dirCount )

    def preen( self, exclude = None ) :
        for cruft in self.cruft() :
            if exclude and cruft in exclude :
                logger.debug( "skipping non-cruft %s" % cruft )
                continue
            cruft = self.getOutputPath( cruft )
            for root, dirs, files in os.walk( cruft, topdown=False ):
                for name in files :
                    if self.root.preenCruft :
                        logger.info( "removing cruft: %s", name )
                        os.remove( os.path.join( root, name ) )
                    else :
                        logger.warn( "cruft found but not removed: %s", name )
                for name in dirs :
                    if self.root.preenCruft :
                        logger.info( "removing cruft: %s", name )
                        os.rmdir( os.path.join( root, name ) )
                    else :
                        logger.warn( "cruft found but not removed: %s", name )
            if self.root.preenCruft :
                logger.info( "removing cruft: %s", cruft )
                if os.path.isfile( cruft ) :
                    os.remove( cruft )
                else :
                    os.rmdir( cruft )
            else :
                logger.warn( "cruft found but not removed: %s", cruft )

    def cruft( self ) :
        trimdir = [ n.outputName for n in self.folderItems ]
        expectedfiles = self.expectedFiles
        ignored = self.ignored + self.root.ignoredCruft
        for file in self.fileItems :
            expectedfiles.extend( file.expectedFilenames )
        for item in os.listdir( self.outputPath ) :
            if item in ignored :
                continue
            itempath = os.path.join( self.outputPath, item )
            if ( os.path.isdir( itempath ) and item not in trimdir ) :
                yield item
            elif ( os.path.isfile( itempath ) and item not in expectedfiles ) :
                yield item

    def summary_item ( self, limit = 4 ) :
        d = Item.preview_item( self )
        seen = set()
        seen.add( self.thumbnailURL )
        thumbs = []
        for i in self.items() :
            preview = i.thumbnailURL
            if preview not in seen :
                seen.add( preview )
                thumbs.append( ( preview, i.itemURL ) )
            if len( thumbs ) >= limit :
                break
        d['peek'] = thumbs
        return d

    def full_item ( self ) :
        d = Item.full_item( self )
        d['children'] = [ folder.summary_item() for folder in  self.folders ]
        d['items'] = [ item.full_item() for item in self.files ]
        d['relateds'] = [ so.preview_item() for so in self.related ]
        return d

    folders = property( getFolders )
    files = property( getFiles )
    related = property( getRelated )
    expectedFiles = property( getExpectedFiles )
    outputURL = property( getOutputURL )
    pageURL = property( getPageURL )
    thumbnailURL = property( getThumbnailURL )
    iconURL = property( getIconURL )
    pageFile = property( getPageFile )
    isDirty = property( checkDirty )
    copyright = property( getCopyright )
    feedback = property( getFeedback )
    subtitle = property( getSubtitle )
    properties = None
    additionalProperties = None
    generatedStylesheets = property( getGeneratedStylesheets )

class RootFolder ( Folder ) :
    def __init__ ( self, sourceDir, outputDir, config ) :
        self.sourceDir = os.path.normpath( os.path.join( os.curdir, os.path.expanduser( sourceDir ) ) )
        self.outputDir = os.path.normpath( os.path.join( os.curdir, os.path.expanduser( outputDir ) ) )
        self.root = self
        self.dirCount = 1
        self.fileCount = 0
        self.dirDeployed = 0
        self.config = config
        self._templateEngine = None
        Folder.__init__( self, "" )

    def getSourcePath ( self, path ) :
        return os.path.join( self.sourceDir, path )

    def getOutputPath ( self, path ) :
        return os.path.join( self.outputDir, path )

    def getRelativePath ( self, path ) :
        return path + "/"

    prefixRE = re.compile( '^.*__' )
    def trimPrefix ( self, name ) :
        return self.prefixRE.sub( '', name )

    def getRelativeSourcePath ( self, path ) :
        return path.replace( self.sourceDir, "" )

    def pathToTop ( self ) :
        return ""

    def getDisplayName ( self ) :
        return self.config['site-title']
    def getDefaultSortOrder ( self ) :
        return self.config['sort-order']
    def getDefaultImageSizes ( self ) :
        return self.config[ 'image-sizes' ]
    def getDefaultImageQuality ( self ) :
        return self.config[ 'image-quality' ]
    def getPreen( self ) :
        return self.config[ 'preen' ]
    def getDefaultCopyright ( self ) :
        return self.config['default-copyright']
    def getDisplayPropertyNames ( self ) :
        return self.config['display-properties']
    def getDefaultFeedback ( self ) :
        return self.config['default-feedback']
    def getAdditionalPropertyNames ( self ) :
        return self.config['additional-properties']
    def getTypeTemplates ( self, filetype ) :
        return self.config[ filetype ][ 'templates' ]
    def getTypeFiles ( self, filetype ) :
        return self.config[ filetype ][ 'files' ]
    def getTypeImageName ( self, filetype, image ) :
        return self.config[ filetype ][ image ]
    @memoize
    def getTemplateSetPath ( self ) :
        if 'template-set' in self.config and self.config['template-set'] :
            return os.path.join( self.config['template-dir'], self.config['template-set'] )
        return self.config['template-dir']
    def getTemplateAssetPath ( self ) :
        return os.path.join( self.getTemplateSetPath(), 'assets' )
    def getGeneratedAssets ( self ) :
        return self.config.get( 'generated-assets', () )
    def getTemplateSchemes ( self ) :
        return self.config.get( 'schemes', {} )
    @memoize
    def getTemplateFilePath( self, filename ) :
        # jinja will load the template from the correct directory. otherwise we would need this
        # return os.path.join( self.getTemplateSetPath(), filename )
        return filename
    def getTemplateEngine ( self ) :
        return self._templateEngine
    def getIgnoredCruft ( self ) :
        return self.config.get( 'ignore-cruft', [] )
    def getConfigTime ( self ) :
        return self.config.get( 'mtime', 0 )
    def getCustomStylesheet ( self ) :
        return self.config.get( 'custom-stylesheet', False )

    @memoize
    def checkDirty( self ) :
        isDirty = Folder.checkDirty( self )
        if not isDirty :
            if not os.path.exists( self.assetPath ) :
                isDirty = True
            else :
                td = self.getTemplateAssetPath()
                generated = list( self.generatedAssets )
                gss = self.generatedStylesheets.values()
                generated.extend( x['stylesheet'] for x in itertools.chain( *gss ) )
                for item in os.listdir( td ) :
                    itempath = os.path.join( self.assetPath, item )
                    if not os.path.exists( itempath ) :
                        isDirty = True
                        break
                    dtime = os.stat( itempath ).st_mtime
                    stime = os.stat( os.path.join( td, item ) ).st_mtime
                    if dtime < stime :
                        isDirty = True
                        break
                    if item in generated and dtime < self.configTime :
                        isDirty = True
                        break

        return isDirty

    def cruft( self ) :
        for i in Folder.cruft( self ) :
            if i != '.assets' :
                yield i
        td = self.getTemplateAssetPath()
        generated = list( self.generatedAssets )
        all_stylesheets = self.generatedStylesheets.values()
        generated.extend( x['stylesheet'] for x in itertools.chain( *all_stylesheets ) )
        for item in os.listdir( self.assetPath ) :
            if item not in generated and not os.path.exists( os.path.join( td, item ) ) :
                yield item

    def deploy( self, templateEngine ) :
        self._templateEngine = templateEngine
        if self.isDirty :
            if not os.path.exists( self.assetPath ) :
                os.makedirs( self.assetPath )
            td = self.getTemplateAssetPath()
            for item in os.listdir( td ) :
                inpath = os.path.join( td, item )
                outpath = os.path.join( self.assetPath, item )
                dtime = os.path.exists( outpath ) and int( os.stat( outpath ).st_mtime ) or 0
                stime = int( os.stat( inpath ).st_mtime )
                if item in self.generatedAssets and ( dtime < self.configTime or dtime < stime ) :
                    logger.debug( "regenerating asset %s to %s", inpath, outpath )
                    self.generateFromTemplate( os.path.join( "assets", item ), outpath )
                elif item in self.generatedStylesheets.keys() :
                    logger.debug( "not copying stylesheet base %s", inpath )
                elif dtime < stime :
                    logger.debug( "copying asset %s to %s.", inpath, outpath )
                    shutil.copy2( inpath, outpath )
            for base_stylesheet, output_stylesheets in self.generatedStylesheets.iteritems() :
                inpath = os.path.join( td, base_stylesheet )
                stime = int( os.stat( inpath ).st_mtime )
                for item in output_stylesheets :
                    outpath = os.path.join( self.assetPath, item['stylesheet'] )
                    dtime = os.path.exists( outpath ) and int( os.stat( outpath ).st_mtime ) or 0
                    if dtime < self.configTime or dtime < stime :
                        scheme = item['scheme']
                        logger.debug( "regenerating stylesheet %s from '%s'(%s)" % ( outpath, inpath, scheme ) )
                        self.generateFromTemplate( os.path.join( "assets", base_stylesheet ), outpath, self.templateSchemes[scheme] )

            if self.customStylesheet :
                inpath = os.path.join( self.sourceDir, self.customStylesheet )
                outpath = os.path.join( td, self.customStylesheet )
                dtime = os.path.exists( outpath ) and int( os.stat( outpath ).st_mtime ) or 0
                stime = os.path.exists( inpath ) and int( os.stat( inpath ).st_mtime ) or 0
                logger.debug( "custom stylesheet" )
                if dtime < stime :
                    logger.debug( "copying custom stylesheet %s to %s.", inpath, outpath )
                    shutil.copy2( inpath, outpath )
            Folder.deploy( self )

    @memoize
    def getExpectedFilenames( self ) :
        l = Folder.getExpectedFilenames( self )
        l.extend( '.assets' )
        return l

    @memoize
    def getAssetPath( self ) :
        return os.path.join( self.outputPath, '.assets' )

    def getOutputURL( self ) :
        return ""

    isDirty = property( checkDirty )
    expectedFilenames = property( getExpectedFilenames )
    assetPath = property( getAssetPath )
    outputURL = property( getOutputURL )
    name = property( getDisplayName )
    defaultSortOrder = property( getDefaultSortOrder )
    defaultCopyright = property( getDefaultCopyright )
    displayPropertyNames = property( getDisplayPropertyNames )
    additionalPropertyNames = property( getAdditionalPropertyNames )
    defaultImageSizes = property( getDefaultImageSizes )
    defaultImageQuality = property( getDefaultImageQuality )
    defaultFeedback = property( getDefaultFeedback )
    preenCruft = property( getPreen )
    templateEngine = property( getTemplateEngine )
    ignoredCruft = property( getIgnoredCruft )
    generatedAssets = property( getGeneratedAssets )
    templateSchemes = property( getTemplateSchemes )
    configTime = property( getConfigTime )
    customStylesheet = property( getCustomStylesheet )

class LeafMixin( object ) :
    def breadcrumbs( self ) :
        return self.parent.breadcrumbs()

    def highlighted( self ) :
        return self

    def loadHeader( self ) :
        self.header = {}

    @memoize
    def getProperties( self ) :
        return self.filterProperties( self.parent.root.displayPropertyNames )

    @memoize
    def getAdditionalProperties( self ) :
        return self.filterProperties( self.parent.root.additionalPropertyNames )

    def getGeneratedStylesheets ( self ) :
        return self.parent.root.config.get( 'generated-stylesheets', {} )

    @requireHeader
    def filterProperties( self, props ):
        ret = []
        header = self.header
        for prop in props :
            prop = prop.split( "@", 1 )
            alts = prop[-1].split( "|" )
            name = prop[0] if len( prop ) > 1 else alts[0]
            for alt in alts :
                if header and alt in header and header[alt]:
                    ret.append( { 'name' : name, 'value' : header[alt] } )
                    break
                elif alt in self.meta and self.meta[alt]:
                    ret.append( { 'name' : name, 'value' : self.meta[alt] } )
                    break
        return ret

    def pathToTop( self ) :
        return self.parent.pathToTop()

    properties = property( getProperties )
    additionalProperties = property( getAdditionalProperties )
    generatedStylesheets = property( getGeneratedStylesheets )

class Reference ( Item ) :
    def __init__ ( self, parent, name, sourcePath ) :
        self.referenceName = name
        self.outputName = parent.root.trimPrefix( name )
        self.sourcePath = sourcePath
        self.sourceName = name
        Item.__init__( self, sourcePath )
        # override the source path's metadata with that associated with the link, if available.
        self.updateMeta( parent.getSourcePath( name ) )
        if 'ignore' in self.meta and ( safeBool( self.meta['ignore'] ) or self.meta['ignore'][:6] == 'ignore' ) :
            raise IgnoreMe()
        self.parent = parent
        self.pathUp = parent.pathToTop()

    @requireActual
    def pathTo( self, url ) :
        return normpath( self.pathUp + self.actual.parent.getRelativePath( '.' ) + url )

    def actualResolved( self ) :
        newMeta = {}
        newMeta.update( self.actual.meta )
        newMeta.update( self.meta )
        self.meta = newMeta

    def resolveActual( self ) :
        # assumes valid path objects have been created
        sourceAbs = os.path.realpath( self.parent.root.sourceDir )
        if self.sourcePath[0:len(sourceAbs)] != sourceAbs :
            raise ValueError, "'%s' is not a prefix of '%s'." % ( sourceAbs, self.sourcePath )
        self.sourcePath = self.sourcePath[len(sourceAbs):]
        pathElements = self.sourcePath.split( os.sep )
        actual = self.parent.root
        for element in pathElements :
            if element :
                actual = actual._get( element ) # use _get to avoid recursively calling resolveActual on dir-local links.
        # if we reach here, we're at the end of the path and have an actual element (failures will result in KeyError)
        self.actual = actual
        self.actualResolved()

    @requireActual
    def getIconURL( self ) :
        return self.pathTo( self.actual.iconURL )

    @requireActual
    def getThumbnailURL( self ) :
        return self.pathTo( self.actual.thumbnailURL )

    @requireActual
    def getDisplayName ( self ) :
        return Item.getDisplayName( self )

    iconURL = property( getIconURL )
    thumbnailURL = property( getThumbnailURL )
    name = property( getDisplayName )

class RelatedItem( Reference ) :
    def __init__ ( self, parent, name, sourcePath ) :
        Reference.__init__( self, parent, name, sourcePath )
        parent.addRelated( self )

    @requireActual
    def _get( self, key ) :
        return self.actual._get( key )

    @requireActual
    def getItemURL ( self ) :
        return self.pathTo( self.actual.getPageURL() )

    @requireActual
    def getSubtitle( self ) :
        if 'subtitle' in self.meta :
            return formatText( self.meta['subtitle'] )
        return self.actual.subtitle

    @requireActual
    def findPath( self, targetPath ) :
        path = targetPath.split( '/', 1 )
        if len( path ) > 1 and path[0] == '..' :
            return self.parent.findPath( path[1] )
        else :
            return self.actual.findPath( targetPath )

    @requireActual
    def highlighted ( self ) :
        try :
            chosen = findEntry( self.meta, "highlight", "sample", "sampleimage" )
            return self.findPath( chosen ).highlighted()
        except KeyError :
            pass
        except :
            logger.error( "related.highlight exception on %s", self.outputName, exc_info = sys.exc_info() )
        return self

    @requireActual
    def checkDirty ( self ) :
        if self.actual.isDirty :
            return True
        #if int( self.meta['mtime'] ) > int( self.parent.meta['mtime'] ) :
        if isFileOlder( self.meta['mtime'], self.parent.getOutputPath( self.parent.pageFile ) ) :
            return True
        return False

    @requireActual
    def collationValue( self, key ) :
        if key == "name" :
            return self.sourceName
        if key == "date" :
            # possible complications if date is in meta?  Not for related items for now.
            return os.stat( self.sourcePath ).st_mtime
        value = Item.collationValue( self, key )
        if value == 0 :
            value = self.actual.collationValue( key )
        return value

    @requireActual
    def getItems ( self ) :
        return self.actual.items

    @requireActual
    def getPreviewItems( self ) :
        return self.actual.preview_items

    itemURL = property( getItemURL )
    pageURL = property( getItemURL )
    isDirty = property( checkDirty )
    subtitle = property( getSubtitle )
    items = property( getItems )
    preview_items = property( getPreviewItems )

class LinkedItem( Reference, LeafMixin ) :
    def __init__ ( self, parent, name, sourcePath ) :
        #self.sourceName = name
        Reference.__init__( self, parent, name, sourcePath )
        self.outputFilename = parent.root.trimPrefix( self.sourceName )
        self.outputPath = parent.getOutputPath( self.outputFilename )
        parent.addFile( self )
        self.expectedFilenames = set()
        self.expectedFilenames.add( self.pageFile )
        try :
            self.mtime = os.stat( parent.getOutputPath( self.pageFile ) ).st_mtime
        except :
            self.mtime = 0

    @requireActual
    def checkDirty( self ) :
        if self.actual.isDirty :
            return True
        if int( self.meta['mtime'] ) > int( self.mtime ) :
            return True
        for f in self.expectedFilenames :
            if isFileNewer( self.mtime, self.parent.getOutputPath( f ) ) :
                return True

    def actualResolved( self ) :
        self.sourceParent = self.actual.parent
        Reference.actualResolved( self )

    @requireActual
    def getSourcePageURL ( self ) :
        return self.pathTo( "../" + self.actual.parent.pageURL )

    @requireActual
    def getSourceThumbnailURL ( self ) :
        return self.pathTo( "../" + self.actual.parent.thumbnailURL )

    @requireActual
    def getSourceIconURL ( self ) :
        return self.pathTo( "../" + self.actual.parent.iconURL )

    @requireActual
    def getSourceDisplayName ( self ) :
        return self.actual.parent.name

    @requireActual
    def getTypeThumbnail( self ) :
        return self.actual.typeThumbnail

    @requireActual
    def getTypeIcon( self ) :
        return self.actual.typeIcon

    @requireActual
    def getCopyright( self ) :
        return self.actual.copyright

    @requireActual
    def getOverlayIcon( self ) :
        return self.actual.overlayIcon

    def getPage( self ) :
        return self.outputFilename + ".html"

    @requireActual
    def getPreview( self ) :
        return self.pathTo( self.actual.previewURL )

    @requireActual
    def getItemURL( self ) :
        return self.pathTo( self.actual.itemURL )

    @requireActual
    def getFiletype( self ) :
        return self.actual.filetype

    @requireActual
    def deploy( self ) :
        # A small subset of File.deploy.
        if self.isDirty :
            self.parent.ensureOutputPath()
            for template, outname in self.parent.root.getTypeTemplates( self.filetype ).iteritems() :
                outname = self.outputFilename + outname
                outfile = self.parent.getOutputPath( outname )
                self.expectedFilenames.add( outname )
                #if self.isFileDirty( outfile ) :
                self.generateFromTemplate( template, outfile )

    @requireActual
    def collationValue( self, key ) :
        if key == "name" :
            return self.sourceName
        return self.actual.collationValue( key )

    isDirty = property( checkDirty )
    pageURL = property( getPage )
    pageFile = property( getPage )
    typeThumbnail = property( getTypeThumbnail )
    typeIcon = property( getTypeIcon )
    copyright = property( getCopyright )
    overlayIcon = property( getOverlayIcon )
    previewURL = property( getPreview )
    itemURL = property( getItemURL )
    filetype = property( getFiletype )
    sourcePageURL = property( getSourcePageURL )
    sourceThumbnailURL = property( getSourceThumbnailURL )
    sourceIconURL = property( getSourceIconURL )
    sourceDisplayName = property( getSourceDisplayName )

class File ( Item, LeafMixin ) :
    def __init__ ( self, parent, name, sourcePath = None ) :
        self.sourceName = name
        self.sourcePath = sourcePath or parent.getSourcePath( self.sourceName )
        Item.__init__( self, self.sourcePath )
        if 'ignore' in self.meta and ( safeBool( self.meta['ignore'] ) or self.meta['ignore'][:6] == 'ignore' ) :
            raise IgnoreMe()
        self.outputFilename = parent.root.trimPrefix( self.sourceName )
        self.outputPath = parent.getOutputPath( self.outputFilename )
        self.outputName, self.extension = self.outputFilename.rsplit( '.', 1 )
        self.stat = os.stat( self.sourcePath )
        self.mtime = self.stat.st_mtime
        self.parent = parent
        parent.addFile( self )

    @memoize
    def checkDirty( self ) :
        isDirty = False
        p = os.path.dirname( self.outputPath )
        for f in self.expectedFilenames :
            expected = os.path.join( p, f )
            if isFileOlder( self.mtime, expected ) :
                if os.path.exists( expected ) :
                    logger.debug( '%s is out of date: %d : %d' % ( expected, os.stat( expected ).st_mtime or 0, self.mtime or 0 ) )
                else :
                    logger.debug( '%s does not exist' % expected )
                isDirty = True
                break
            d_stat = os.stat( expected )
            if expected == self.outputPath and self.stat.st_size != d_stat.st_size :
                logger.debug( "%s is the incorrect size", expected )
                isDirty = True
                break
            elif f == self.pageFile and int( self.meta['mtime'] ) > int( d_stat.st_mtime ) :
                logger.debug( "%s has a meta-data change", expected )
                isDirty = True
                break
        return isDirty

    def collationValue( self, key ) :
        if key == "name" :
            return self.sourceName
        if key == "date" :
            return self.stat.st_mtime
        if key == "size" :
            return self.stat.st_size
        return Item.collationValue( self, key )

    def getExpectedFilenames( self ) :
        return [ self.outputName ]

    @requireHeader
    def deploy( self ) :
        if self.isDirty :
            printed = False
            self.parent.ensureOutputPath()
            if isFileOlder( self.mtime, self.outputPath ) or isFileDifferent( self.sourcePath, self.outputPath ) :
                shutil.copy2( self.sourcePath, self.outputPath )
                printed = logPartial( self.filetype )
            if self.thumbnailSource :
                for name in self.parent.root.getTypeFiles( self.filetype ) :
                    outname = self.parent.getOutputPath( self.outputName + "." + name + ".jpg" )
                    if isFileOlder( self.mtime, outname ) :
                        generateImage( self.thumbnailSource, outname, self.parent.root.defaultImageSizes[name], self.parent.root.defaultImageQuality, panorama = self.isPanorama )
                        printed = logPartial( name )
            for template, outname in self.parent.root.getTypeTemplates( self.filetype ).iteritems() :
                outfile = self.parent.getOutputPath( self.outputFilename + outname )
                if isFileOlder( self.mtime, outfile ) or isFileOlder( self.meta['mtime'], outfile ):
                    self.generateFromTemplate( template, outfile )
            if printed :
                print "done", self.outputFilename

    def getPage( self ) :
        return self.outputFilename + ".html"

    def getTypeThumbnail( self ) :
        return self.parent.pathToTop() + ".assets/" + self.parent.root.getTypeImageName( self.filetype, 'thumbnail' )

    def getTypeIcon( self ) :
        try :
            return self.parent.pathToTop() + ".assets/" + self.parent.root.getTypeImageName( self.filetype, 'icon' )
        except :
            return None

    def getOverlayIcon( self ) :
        return None

    def getItemURL( self ) :
        return self.outputFilename

    def getCopyright( self ) :
        if 'copyright' in self.meta :
            return self.meta['copyright']
        if 'copyright' in self.header :
            return self.header['copyright']
        return self.parent.copyright

    def getFeedback( self ) :
        if 'feedback' in self.meta :
            return self.meta['feedback']
        return self.parent.feedback

    def getPanorama( self ) :
        if 'panorama' in self.meta :
            return safeBool( self.meta['panorama'] )
        return False

    isDirty = property( checkDirty )
    pageURL = property( getPage )
    pageFile = property( getPage )
    typeThumbnail = property( getTypeThumbnail )
    typeIcon = property( getTypeIcon )
    copyright = property( getCopyright )
    feedback = property( getFeedback )
    overlayIcon = property( getOverlayIcon )
    itemURL = property( getItemURL )
    isPanorama = property( getPanorama )

class Image ( File ) :
    filetype = "image"

    def loadHeader( self ) :
        self.header = {}
        f = self.sourcePath
        i = PIL.Image.open( f )
        self.meta['width'], self.meta['height'] = i.size
        self.meta['size'] = "%d x %d" % ( i.size )
        self.meta['filesize'] = os.stat( f ).st_size
        if 'exif' in i.info :
            self.exif = i._getexif()
            for tag, value in self.exif.items():
                decoded = get_exif_tag( tag )
                if type( value ) is tuple and len( value ) == 2 and value[1] != 0 :
                    try :
                        value = float( value[0] ) / float( value[1] )
                    except :
                        pass
                self.header[decoded] = terbium.util.decode( str( value ) )

    @memoize
    @requireHeader
    def collationValue( self, key ) :
        if key == "date" :
            # """Data format is "YYYY:MM:DD HH:MM:SS"+0x00, total 20bytes.""""
            for entry in [ "DateTimeOriginal", "DateTimeDigitized", "DateTime" ] :
                if entry in self.header :
                    v = self.header[entry].strip()
                    if v :
                        # parse out the date/time.
                        return time.mktime( datetime.strptime( v, "%Y:%m:%d %H:%M:%S" ).timetuple() )
            return self.stat.st_mtime
        return File.collationValue( self, key )

    def getThumbnailSource( self ) :
        return self.sourcePath

    def getThumbnail( self ) :
        return self.outputName + ".thumb.jpg"

    def getIcon( self ) :
        return self.outputName + ".icon.jpg"

    def getPreview( self ) :
        return self.outputName + ".preview.jpg"

    def getExpectedFilenames( self ) :
        l = [ "%s.%s.jpg" % ( self.outputName, key ) for key in self.parent.root.defaultImageSizes.keys() ]
        l.extend( [ "%s%s" % ( self.outputFilename, key ) for key in self.parent.root.getTypeTemplates( self.filetype ).values() ] )
        l.append( self.outputFilename )
        return l

    thumbnailSource = property( getThumbnailSource )
    thumbnailURL = property( getThumbnail )
    iconURL = property( getIcon )
    previewURL = property( getPreview )
    thumbnailFile = property( getThumbnail )
    iconFile = property( getIcon )
    previewFile = property( getPreview )
    expectedFilenames = property( getExpectedFilenames )

class MediaFile ( File ) :
    @memoize
    def getThumbnailSource( self ) :
        if 'thumbnail' in self.meta :
            return self.meta['thumbnail']
        if os.path.exists( self.sourcePath + ".thm" ) :
            return self.sourcePath + ".thm"
        logger.debug( "No thumbnail found for media file " + self.sourcePath )
        return None

    def getOverlayIcon( self ) :
        if self.thumbnailSource :
            return self.typeIcon

    def getPreview( self ) :
        source = self.thumbnailSource
        if source :
            return self.outputName + ".preview.jpg"
        return self.typeThumbnail

    def getThumbnail( self ) :
        source = self.thumbnailSource
        if source :
            return self.outputName + ".thumb.jpg"
        return self.typeThumbnail

    def getIcon( self ) :
        source = self.thumbnailSource
        if source :
            return self.outputName + ".icon.jpg"
        return self.typeIcon

    def getExpectedFilenames( self ) :
        l = [ "%s.%s.jpg" % ( self.outputName, key ) for key in self.parent.root.defaultImageSizes.keys() ]
        l.extend( [ "%s%s" % ( self.outputFilename, key ) for key in self.parent.root.getTypeTemplates( self.filetype ).values() ] )
        l.append( self.outputFilename )
        return l

    previewURL = property( getPreview )
    thumbnailURL = property( getThumbnail )
    iconURL = property( getIcon )
    previewFile = property( getPreview )
    thumbnailFile = property( getThumbnail )
    iconFile = property( getIcon )
    thumbnailSource = property( getThumbnailSource )
    expectedFilenames = property( getExpectedFilenames )
    overlayIcon = property( getOverlayIcon )

class Video ( MediaFile ) :
    filetype = "video"

class Audio ( MediaFile ) :
    filetype = "audio"

def generateImage( sourceFile, outFile, size, quality = 70, exif = {}, panorama = False ) :
    image = PIL.Image.open( sourceFile )
    width, height = image.size
    if panorama :
        size = size / 2
    if width > height and not panorama :
        w = size
        h = int( float(size) * ( float( height ) / float( width ) ) )
    else :
        h = size
        w = int( float(size) * ( float( width ) / float( height ) ) )
    if ( image.mode == 'P' ) :
        image.draft( "RGB", image.size )
        image = image.convert( "RGB" )
    if ( width > w ) and ( height > h ) :
        # only scale *down*.  Leave already smaller images at their original size.
        out = image.resize( (w, h), PIL.Image.ANTIALIAS )
    else :
        out = image.copy()
    # TODO: exif data copy
    out.save( outFile, quality = quality, optimize = True, progressive = True )

def logPartial( *args ) :
    for arg in args :
        print arg,
    sys.stdout.flush()
    return True

def Skip( *args, **kwargs ) :
    pass

def file_handler( path ) :
    if path[-1] == '~' : return Skip
    if path == '.htaccess' : return Skip
    base, extension = os.path.splitext( path )
    itemHandler = itemTypes.get( extension.lower(), None )
    return itemHandler

def handle_item( parent, item, itempath ) :
    if os.path.islink( itempath ) :
        return handle_link( parent, item, itempath )
    elif os.path.isdir( itempath ) :
        return Folder( item, parent )
    elif os.path.isfile( itempath ) :
        handler = file_handler( item )
        try :
            return handler( parent, item, os.path.islink( itempath ) and os.path.realpath( itempath ) )
        except :
            logger.debug( "`%s`" % item )
            raise

def handle_link( parent, name, actualPath ) :
    while os.path.islink( actualPath ) :
        actualPath = os.path.join( os.path.dirname( actualPath ), os.readlink( actualPath ) )
        if pathInSource( parent, actualPath ) :
            break
    actualPath = os.path.normpath( actualPath )
    if pathInSource( parent, actualPath ) :
        # internal link.
        if os.path.isdir( actualPath ) :
            return RelatedItem( parent, name, actualPath )
        elif os.path.isfile( actualPath ) :
            handler = file_handler( actualPath )
            if handler and handler != Skip :
                # This may not work for nested internal links.
                return LinkedItem( parent, name, actualPath )
    elif pathInTarget( parent, actualPath ) :
        logger.warn( "ignoring link to target directory: %s", actualPath )
        raise IgnoreMe
    else :
        # link to outside the source
        realPath = os.path.realpath( actualPath )
        if os.path.isfile( realPath ) :
            handler = file_handler( realPath )
            try :
                return handler( parent, name, realPath )
            except :
                logger.debug( "skipping %s", realPath )
        elif os.path.isdir( realPath ) :
            # this is the tricky bit and is not yet supported
            logger.warn( "ignoring link to external directory %s", realPath )

def link_file( parent, name, ignored = None ) :
    """handle the contents of a 'shortcut' file"""
    sourcePath = parent.getSourcePath( name )
    meta = {}
    meta['mtime'] = os.stat( sourcePath ).st_mtime
    properties = ConfigParser()
    properties.readfp( codecs.open( sourcePath, "rb", "utf-8" ) )
    if ( properties.has_section( "meta" ) ) :
        meta.update( properties.items( "meta" ) )
    try :
        actualPath = properties.get( "link", "file" )
    except ConfigParser.NoOptionError :
        logger.warn( "ignoring empty link file %s", sourcePath )
        raise IgnoreMe
    actualPath = os.path.normpath( os.path.join( parent.getSourcePath( '.' ), os.path.expanduser( actualPath ) ) )
    item = handle_link( parent, name, actualPath )
    if item :
        item.meta.update( meta )
    logger.debug( "link %s <%s> --> %s", name, actualPath, str(item) )
    return item

def pathInSource ( parent, path ) :
    source = (parent.root or parent).getSourcePath( "" ) # ensure trailing separator
    path = os.path.join( os.curdir, path )
    return os.path.commonprefix( [ source, path ] ) == source

def pathInTarget ( parent, path ) :
    target = (parent.root or parent).getOutputPath( "" ) # ensure trailing separator
    path = os.path.join( os.curdir, path )
    return os.path.commonprefix( [ target, path ] ) == target

itemTypes = {
    ".jpg": Image
    , ".jpeg" : Image
    , ".png" : Image
    , ".gif" : Image
    , ".bmp" : Image
    , ".avi" : Video
    , ".mov" : Video
    , ".mpg" : Video
    , ".mpeg" : Video
    , ".wav" : Audio
    , ".mp3" : Audio
    , ".ogg" : Audio
    , '.rm' : Video
    , '.ra' : Audio
    , '.wma' : Audio
    , '.wmv' : Video
    , '.xml' : Skip
    , '.properties' : Skip
    , '.html' : Skip
    , '.thm' : Skip
    , '.json' : Skip
    , '.css' : Skip
    , '.link' : link_file
    , '.htaccess' : Skip # for now
}
