#!/usr/bin/python
#    terbium/gallery.py - static gallery generation
#    Copyright (C) 2009 Shawn Sulma <shawn@terbium.ca> http://terbium.ca/
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os
import sys
import codecs
import logging
try :
    from pkg_resources import resource_filename, resource_string
except :
    # setuptools not available, assume that the installation is purely local.
    def resource_filename( ignored, filename ) :
        return os.path.join( sys.path[0], filename )
    def resource_string( ignored, filename ) :
        return u"\n".join( l for l in codecs.open( resource_filename( ignored, filename ), "rb", "UTF-8" ) )

import jinja2
try :
    import simplejson as json
except :
    import json

import terbium
import terbium.util
import terbium.template

logger = logging.getLogger ( "terbium" )
logger.setLevel( logging.DEBUG )
handler = logging.StreamHandler()
handler.setLevel( logging.DEBUG )
formatter = logging.Formatter( u"%(levelname)s\t%(message)s" )
handler.setFormatter( formatter )
logger.addHandler( handler )

APP_NAME = 'terbium'
APP_VERSION = '0.5'
APP_URL = 'http://terbium.ca/'

def usage() :
    print "USAGE:"
    print sys.argv[0], "<source-directory>", "<target-directory>"

def readBaseConfiguration( sourceDir ) :
    config = json.loads( resource_string( APP_NAME, 'terbium.json' ) )
    config['template-dir'] = resource_filename( APP_NAME, config['template-dir'] )
    config['mtime'] = os.stat( resource_filename( APP_NAME, 'terbium.json' ) ).st_mtime
    for f in ( os.path.expanduser( '~/.terbium.json' ), os.path.join( sourceDir, 'terbium.json' ) ) :
        if os.path.isfile( f ) :
            config.update ( json.load( codecs.open( f, "rb", "utf-8" ) ) )
            config['mtime'] = max( config['mtime'], os.stat( f ).st_mtime )
    if len( config ) == 0 :
        logger.error( "no project configuration file found" )
        exit( 1 )
    return config

def checkCustomStylesheet( config, sourceDir ) :
    if "custom-stylesheet" in config :
        # custom stylesheet, but only if the file exists.
        p = os.path.join( sourceDir, config['custom-stylesheet'] )
        logger.debug( "checking custom stylesheet %s", p )
        if not ( os.path.exists( p ) and os.path.isfile( p ) ) :
            del config['custom-stylesheet']

def readTextConfiguration( config ) :
    try :
        cfn = config['customization-file']
    except KeyError :
        logger.error( "no text customization-file found" )
        exit( 1 )
    else :
        return terbium.util.loadText( cfn )

def readTemplateConfiguration( config ) :
    tf = os.path.join( config['template-dir'], config.get( 'template-set', '.' ), "template.json" )
    tc = json.load( codecs.open( tf, "rb", "utf-8" ) )
    templateConfig = {}
    templateConfig['generated-assets'] = tc.get( 'generated-assets', () )
    # a colours dict in a configuration file may be a partial definition, relying on defaults or a specific scheme for completion
    colours = 'colours' in config or {}
    # a colour scheme is always a sufficiently-complete dict of colour names
    if "colour-scheme" in config and config['colour-scheme'] in tc['schemes'] :
        scheme = config['colour-scheme']
    else :
        scheme = tc['colour-scheme']
    colours.update( tc['schemes'][scheme] )
    templateConfig['colours'] = colours
    templateConfig['schemes'] = tc.get( 'schemes', {} )
    templateConfig['generated-stylesheets'] = tc.get( 'generated-stylesheets', {} )
    return templateConfig

def generate() :
    try :
        sourceDir = sys.argv[1]
        if sourceDir[-1] != os.path.sep :
            sourceDir += os.path.sep
        outputDir = sys.argv[2]
    except IndexError :
        usage()
        exit()
    sourceDir = os.path.realpath( sourceDir )
    outputDir = os.path.realpath( outputDir )
    if os.path.commonprefix( ( sourceDir, outputDir ) ) == sourceDir :
        print "target directory cannot be a sub-directory of the source directory."
        exit( 1 )

    config = readBaseConfiguration( sourceDir )
    checkCustomStylesheet( config, sourceDir )
    text = readTextConfiguration( config )
    config.update( readTemplateConfiguration( config ) )

    config['APP_NAME'] = APP_NAME
    config['APP_VERSION'] = APP_VERSION
    config['APP_URL'] = APP_URL
    if config.get( 'debug', 0 ) :
        logger.setLevel( logging.DEBUG )
    else :
        logger.setLevel( logging.INFO )

    root = terbium.RootFolder( sourceDir, outputDir, config )

    logger.debug( "%d directories with %d files", root.dirCount, root.fileCount )

    jinja_env = jinja2.Environment( loader = jinja2.FileSystemLoader( os.path.join( config['template-dir'], config.get( 'template-set', '.' ) ) ), extensions = [terbium.template.TextExtension, terbium.template.CleanExtension] )
    jinja_env.text = text
    root.deploy( jinja_env )

if __name__ == "__main__" :
    generate()
