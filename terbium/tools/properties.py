#!/usr/bin/python
#    set-property.py - utility to manage .properties files for terbium static gallery generation
#    Copyright (C) 2009 Shawn Sulma <shawn@terbium.ca>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import os.path
from ConfigParser import ConfigParser
import terbium.util
import codecs
from terbium.util import decode

def set_property():
    try: 
        item = sys.argv[1]
        if os.path.isdir( item ) and not os.path.islink( item ):
            item = os.path.join( item, "album" )
        properties = ConfigParser()
        propfile = item + ".properties"
        if os.path.exists( propfile ) and os.path.isfile( propfile ) :
            properties.read( propfile )
        else :
            properties.add_section( "meta" )
        key = decode( sys.argv[2] )
    except IndexError :
        print "current properties in", item
        for key, value in properties.items( "meta" ) :
            print key, "=", value
    else :
        try :
            value = unicode( decode( sys.argv[3] ) )
        except IndexError :
            value = None
        #xmldata = loadXML( item )
        if properties.has_option( "meta", key ) :
            oldvalue = properties.get( "meta", key )
        else :
            oldvalue = None
        change = True
        if value is not None and value != oldvalue :
            properties.set( "meta", key, value )
        elif value is None and oldvalue is not None:
            properties.remove_option( "meta", key )
        else :
            change = False
        if change :
            of = codecs.open( propfile, "wb", "utf-8" )
            try :
                properties.write( of )
            finally :
                of.close()
            print item, ":", key, "=", oldvalue, "=>", value

if __name__ == '__main__' :
    set_property()

def loadXML( base ) :
    result = []
    f = base + '.xml'
    if os.path.exists( f ) and os.path.isfile( f ) :
        xmlfile = file( f )
        try :
            parser = terbium.util.MetadataXMLHandler( xmlfile )
        finally :
            xmlfile.close()
        result.update( parser.metadata )
    return result
