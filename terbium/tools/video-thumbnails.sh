#!/bin/bash
#
#    video-thumbnails.sh - utility to generate thumbnails for video files in a source directory
#    Copyright (C) 2009 Shawn Sulma <shawn@terbium.ca>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
if [ -z "${1}" ] ; then
	echo "usage ${0} <source directory>"
else
	find "${1}" -iname "*.avi" -or -iname "*.mov" -or -iname "*.as?" -or -iname "*.wmv" -or -iname "*.rm" -or -iname "*.ogg" \
	| while read f ; do
		g="${f}.thm" 
		if [ \! -f "${g}" ] ; then
			echo "--> generating thumbnail for |${f}|"
			# try to grab a frame 5 seconds in as a preview frame
			mplayer -nosound -vo jpeg:progressive:quality=85 -ss 5 -frames 1 "${f}" 
			if [ \! -f "00000001.jpg" ] ; then
				# decoding error, most likely a seek error.  Try again, but grab only first frame
				mplayer -nosound -vo jpeg:progressive:quality=85 -frames 1 "${f}" 
			fi
			if [ -f "00000001.jpg" ] ; then
				mv -iv 00000001.jpg "${g}" 
			fi
		fi
	done
fi
