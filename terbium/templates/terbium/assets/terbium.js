/*
#    terbium.js - javascript helpers for the terbium static gallery generator
#    Copyright (C) 2009 Shawn Sulma <shawn@terbium.ca> http://terbium.ca/
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
var ua = navigator.userAgent.toLowerCase();
var mobile = [ 'iphone', 'ipod', 'symbian', 'windows ce', 'android' ] ;

function isMobile() {
	for ( c in mobile ) {
		if ( ua.search( mobile[c] ) > -1 ) {
			return true;
		}
	}
	return false;
}
function contains( self, node ) {
	if ( node == null ) {
		return false;
	}
	if ( node == self ) {
		return true;
	}
	return contains( self, node.parentNode );
}
function showMe( show, hide ) {
	if ( ! isMobile() ) {
		hideElement( hide );
		showElement( show );
		return true;
	}
	return false;
}
function hideElement( hide ) {
    if ( hide ) {
        document.getElementById( hide ).style.display = 'none';
    }

}
function showElement( show, display ) {
    if ( show ) {
        document.getElementById( show ).style.display = display ? display : 'block';
    }
}

function setMyImage( id, location ) {
    var e = document.getElementById( id );
    if ( e.src != location ) {
	    //var cl = e.parentNode.className
	    f = function() {
		this.parentNode.className = ( this.offsetWidth > 40 ) ? "thumbnail" : "icon" ;
            };
	    e.onload = f;
	    e.src = location;
    }
}
function linkEnter( self, event, id ) {
    showElement( id + ".thumb", "inline" );
    hideElement( id + ".icon" );
}
function linkExit( self, event, id ) {
    showElement( id + ".icon", "inline" );
    hideElement( id + ".thumb" );
}
function menuExit( self, event ) {
}
