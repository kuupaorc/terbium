/*
#    scrubber.js - dynamic scrubber bar generator for the terbium static gallery generation tool
#    Copyright (C) 2012- Shawn Sulma <shawn@terbium.ca> http://terbium.ca/
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// naive first attempt. Assuming folder data is in scope

var last_scrubber_id = 0;
var current_scrubber_id = 0;

function for_all( list, callback ) {
    var a = "";
    if ( list ) {
        for ( var ix in list ) {
            a += callback( list[ix] );
        }
    }
    return a;
}

function scrubber_img( item, current ) {
	var _id = "id='scrubber_" + ( ++last_scrubber_id ) + "'";
	var extra =  "";
	if ( current && item['small'] == current ) {
		extra =  " current";
		current_scrubber_id = last_scrubber_id;
	}
    var orama = item.panoramic ? " panorama" : "";
    var a = "<img " + _id + " class='scrubber" + orama + extra + "' src='" + item['small'] + "'>";
    if ( item.overlay ) {
        a += "<img class='overlay' src='" + item['overlay'] + "'>";
    }
    return a;
}

function build_scrubber_folder( folder ) {
    var a = "<li class='folder'>";
    a += "<a href='" + folder['url'] + "'>";
    a += scrubber_img( folder );
    a += "<div class='caption'><div class='name'>" + folder['name'] + "</div></div>";
    a += "</a></li>";
    return a;
}

function build_scrubber_item( item, current ) {
    var a = "<li class='item'><a href='" + item['url'] + "'>";
    a += scrubber_img( item, current );
    a += "</a></li>";
    return a;
}

function build_scrubber_related( related ) {
    var a = "<li class='related'>";
    a += "<a href='" + related['url'] + "'>";
    a += scrubber_img( related );
    a += "<div class='caption'><div class='title'>" + related['name'] + "</div></div>";
    a += "</a></li>";
    return a;
}

function build_scrubber( current ) {
    var a = for_all( folder_data.children, build_scrubber_folder );
    a += for_all( folder_data.items, function( item ) { return build_scrubber_item( item, current ) } );
    a += for_all( folder_data.relateds, build_scrubber_related );
    return a;
}
function toggle_scrubber() {
    var sw = document.getElementById( "scrubber-handle" );
    sw.classList.toggle( "active" );
    sw = document.getElementById( "scrubber-content" );
    sw.classList.toggle( "active" );
}
function scrubber_up() {
    var sw = document.getElementById( "scrubber-handle" );
    sw.classList.add( "active" );
    sw = document.getElementById( "scrubber-content" );
    sw.classList.add( "active" );
}
function scrubber_down() {
    var sw = document.getElementById( "scrubber-handle" );
    sw.classList.remove( "active" );
    sw = document.getElementById( "scrubber-content" );
    sw.classList.remove( "active" );
}
function best_height( desired_height ) {
    var image = document.getElementById( "image" );
    if ( desired_height > image.naturalHeight ) {
        return best_height( image.naturalHeight );
    }
    if ( ( window.innerWidth * 0.9 ) < ( desired_height * ( image.naturalWidth / image.naturalHeight ) ) ) {
        return ( window.innerWidth * 0.9 ) * ( image.naturalHeight / image.naturalWidth );
    }
    return desired_height;
}

function scroll_scrubber( c ) {
    var current = c.querySelector(".current");
    if ( ! current && current_scrubber_id > 0 ) {
        current = document.getElementById( "scrubber_" + current_scrubber_id );
    }
    if ( current ) {
        var target = current.parentNode.parentNode;
        var xOffset = target.offsetLeft - ( c.clientWidth / 2 ) + target.offsetWidth / 2;
        c.scrollLeft = xOffset;
    }
}

function resize_visual() {
    var a = document.getElementById( 'item' );
    var b = document.getElementById( 'item-container' );
    var i = document.getElementById( 'image' );
    var footer = document.getElementById( "footer" );
    if ( a && b && footer ) {
        var top = a.offsetTop;
        var bottom = ( footer.offsetTop + footer.offsetHeight ) - ( a.offsetTop + a.offsetHeight );
        var chrome = top + bottom;
        var height = ( window.innerHeight - chrome - 20 );
        b.style.height = best_height( height ) + "px";
    }
};
