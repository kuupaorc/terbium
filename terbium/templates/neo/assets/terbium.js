/*
#    terbium.js - javascript helpers for the terbium static gallery generator
#    Copyright (C) 2009 Shawn Sulma <shawn@terbium.ca> http://terbium.ca/
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
var ua = navigator.userAgent.toLowerCase();
var mobile = [ 'iphone', 'ipod', 'symbian', 'windows ce', 'android' ] ;

function isMobile() {
	for ( c in mobile ) {
		if ( ua.search( mobile[c] ) > -1 ) {
			return true;
		}
	}
	return false;
}
function is_touch_device() {
    return 'ontouchstart' in window // works on most browsers 
        || 'onmsgesturechange' in window; // works on ie10
}
function contains( self, node ) {
	if ( node == null ) {
		return false;
	}
	if ( node == self ) {
		return true;
	}
	return contains( self, node.parentNode );
}
function showMe( show, hide ) {
	if ( ! isMobile() ) {
		hideElement( hide );
		showElement( show );
		return true;
	}
	return false;
}
function hideElement( hide ) {
    if ( hide ) {
        document.getElementById( hide ).style.display = 'none';
    }

}
function showElement( show, display ) {
    if ( show ) {
        document.getElementById( show ).style.display = display ? display : 'block';
    }
}
function toggleElement( id, key ) {
	var e = document.getElementById( id );
	if ( e ) {
		e.classList.toggle("hidden");
	}
	if ( key ) {
		try {
			localStorage.setItem( key, e.classList.contains( "hidden" ) ? "false" : "true" );
		}
		catch( err ) {}
	}
}

function toggle_zoom( obj ) {
    obj.classList.add( "zooming" );
    obj.classList.toggle( "zoomed" );
    setTimeout( function() { obj.classList.remove( "zooming" ) }, 200 );
}

function setMyImage( id, location ) {
    var e = document.getElementById( id );
    if ( e.src != location ) {
	    //var cl = e.parentNode.className
	    f = function() {
		this.parentNode.className = ( this.offsetWidth > 40 ) ? "thumbnail" : "icon" ;
            };
	    e.onload = f;
	    e.src = location;
    }
}
function linkEnter( self, event, id ) {
    showElement( id + ".thumb", "inline" );
    hideElement( id + ".icon" );
}
function linkExit( self, event, id ) {
    showElement( id + ".icon", "inline" );
    hideElement( id + ".thumb" );
}
function menuExit( self, event ) {
}

function touching() {
    var touch = is_touch_device();
    var a = document.querySelectorAll( ".touch" );
    for( var ix in a ) {
        var e = a[ix];
	if ( e.classList == undefined ) {
		continue;
	}
	if ( touch ) {
		e.classList.remove("notouch");
		e.classList.add("touch");
	}
	else {
		e.classList.remove("touch");
		e.classList.add("notouch");
	}
    }
}

function toggle_item( self ) {
    if ( is_touch_device() ) {
	if ( itemURL ) {
	    window.location = itemURL;
	}
    }
    else {
	toggle_zoom( self );
    }
}

// hacky minimal swipe gestures.
var minLength = 64; // the shortest distance the user may swipe

// <div id="picture-frame" ontouchstart="touchStart(event,'picture-frame');"  ontouchend="touchEnd(event);" ontouchmove="touchMove(event);" ontouchcancel="touchCancel(event);">
function attach_gestures( self ) {
    var touches = "ontouchend" in self;
    var touchCancel = function( event, quiet ) {
        self._touch_startX = 0;
        self._touch_startY = 0;
        self._touch_curX = 0;
        self._touch_curY = 0;
	self._fingers = 0;
	if ( ! quiet && "onGestureEnd" in self ) {
		self["onGestureEnd"];
	}
    };
    var touchStart = function( event ) {
        self._fingers = touches ? event.touches.length : 1;
	if ( self._fingers == 1 ) {
	    event.preventDefault();
	    self._touch_startX = touches ? event.touches[0].pageX : event.pageX;
	    self._touch_startY = touches ? event.touches[0].pageY : event.pageY;
	    if ( "onGestureStart" in self ) {
		self["onGestureStart"]( self._touch_startX, self._touch_startY );
	    }
        } 
	else {
            touchCancel( event );
	}
    };
    var touchMove = function( event ) {
	if ( self._fingers == 1 ) {
            event.preventDefault();
            self._touch_curX = touches ? event.touches[0].pageX : event.pageX;
            self._touch_curY = touches ? event.touches[0].pageY : event.pageY;
	    if ( "onGestureDrag" in self ) {
		self["onGestureDrag"]( self._touch_curX, self._touch_curY );
	    }
        } 
        else {
            touchCancel( event );
        }
    };
    var touchEnd = function( event ) {
	var quiet = false;
        if ( self._fingers == 1 && self._touch_curX != 0 ) {
	    var dx = self._touch_curX - self._touch_startX;
	    var dy = self._touch_curY - self._touch_startY;
            var swipeLength = Math.round( Math.sqrt( Math.pow( dx, 2 ) + Math.pow( dy, 2 ) ) );
            if ( swipeLength >= minLength ) {
                event.preventDefault();
		var result = null;
		if ( Math.abs( dx ) > Math.abs( dy ) ) {
		    result = dx > 0 ? "onSwipeLeft" : "onSwipeRight";
		}
		else {
		    result = dy > 0 ? "onSwipeDown" : "onSwipeUp";
		}
		if ( result in self) {
			quiet = true;
			self[result]();
		}
		else if ( "onGestureFail" in self ) {
			quiet = true;
			self["onGestureFail"]();
		}
	    }
	}
	touchCancel( event, quiet );
    };
    touchCancel();
    if ( "ontouchend" in self ) {
	self.ontouchstart = touchStart;
	self.ontouchend = touchEnd;
	self.ontouchmove = touchMove;
	self.ontouchcancel = touchCancel;
    }
    else {
	self.onmousedown = touchStart;
	self.onmouseup = touchEnd;
	self.onmousemove = touchMove;
    }
}
